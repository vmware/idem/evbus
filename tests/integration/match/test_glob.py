def test_find(hub):
    assert hub.match.glob.find(name="idem-low", pattern="idem-*")


def test_exact(hub):
    assert hub.match.glob.find(name="idem-low", pattern="idem-low")


def test_lost(hub):
    assert not hub.match.glob.find(name="idem-low", pattern="idem-")
