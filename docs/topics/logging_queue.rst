Logging Queue
=============

defaults
--------
Log messages will be put on the event bus the routing key ``log`` and the ingress profile ``logger``

.. code-block:: bash

    $ ./my_pop_app.py --log-level=debug --log-plugin=queue

Any ingress queue that should receive log messages needs to be configured with a "logger" profile:

.. code-block:: sls

    internal:
      logger:
    my_ingress_provider:
      logger:
        my_ingress_param: value

customization
-------------

You can specify your own ingress profile for log messages on the CLI like so:
.. code-block:: bash

    $ ./my_pop_app.py --log-level=debug --log-plugin=queue --log-handler-options profile=my_ingress_profile

Any ingress queue that should receive log messages needs to be configured with your custom profile:

.. code-block:: sls

    my_ingress_provider:
      my_ingress_profile:
        my_ingress_param: value
