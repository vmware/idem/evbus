import json
import unittest.mock as mock


async def test_defaults(hub, event_loop):
    hub.pop.loop.CURRENT_LOOP = event_loop

    with mock.patch("sys.argv", ["evbus", "--log-level=error", "--log-plugin=queue"]):
        hub.pop.config.load(["evbus", "acct", "rend"], cli="evbus")

    hub.log.error("test: Added before listener")

    contexts = {"internal": [{"logger": {"routing_key": "log"}}]}
    t = hub.pop.Loop.create_task(hub.evbus.init.start(contexts))
    hub.evbus.init.join()

    await hub.pop.loop.sleep(0)

    hub.log.error("test: Added after listener")

    await hub.pop.loop.sleep(0)

    # Get the internal queue that should have been created by now
    queue = hub.ingress.internal.QUEUE["log"]
    # Verify that both logs were propagated to the queue
    before = json.loads(await queue.get())
    assert before["level"] == "ERROR"
    assert "Added before listener" in before["message"]
    after = json.loads(await queue.get())
    assert after["level"] == "ERROR"
    assert "Added after listener" in after["message"]

    await hub.evbus.init.stop()
    await t


async def test_handler_options(hub, event_loop):
    hub.pop.loop.CURRENT_LOOP = event_loop

    with mock.patch(
        "sys.argv",
        [
            "evbus",
            "--log-level=error",
            "--log-plugin=queue",
            "--log-handler-options",
            "profile=my_profile",
        ],
    ):
        hub.pop.config.load(["evbus", "acct", "rend"], cli="evbus")

    hub.log.error("test: Added before listener")

    contexts = {"internal": [{"my_profile": {"routing_key": "log"}}]}
    t = hub.pop.Loop.create_task(hub.evbus.init.start(contexts))
    hub.evbus.init.join()
    await hub.pop.loop.sleep(0)

    hub.log.error("test: Added after listener")

    await hub.pop.loop.sleep(0)

    # Get the internal queue that should have been created by now
    queue = hub.ingress.internal.QUEUE["log"]
    # Verify that both logs were propagated to the queue
    before = json.loads(await queue.get())
    assert before["level"] == "ERROR"
    assert "Added before listener" in before["message"]
    after = json.loads(await queue.get())
    assert after["level"] == "ERROR"
    assert "Added after listener" in after["message"]

    await hub.evbus.init.stop()
    await t
